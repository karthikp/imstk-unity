﻿using UnityEngine;

namespace Imstk
{
    public static class Utility
    {
        // Trys to get a component, if it doesn't exists, it creates one
        public static T GetComponentOrCreate<T>(this GameObject gameObj) where T : Component
        {
            T comp = gameObj.GetComponent<T>();
            if (comp == null)
                return gameObj.AddComponent<T>();
            else
                return comp;
        }
        // Trys to get a component, if it doesn't exists, throws exception
        public static T GetComponentFatal<T>(this GameObject gameObj) where T : Component
        {
            T comp = gameObj.GetComponent<T>();
            if (comp == null)
                throw new MissingComponentException();
            return comp;
        }

        public static Geometry GetTetCubeMesh()
        {
            // Make a tetrahedral cube
            Geometry cubeTetMesh = ScriptableObject.CreateInstance<Geometry>();
            cubeTetMesh.name = "CubeTetMesh";
            cubeTetMesh.type = GeometryType.TetrahedralMesh;
            Vector3[] vertices = {
                new Vector3(-0.5f, 0.5f, -0.5f),
                new Vector3(0.5f, 0.5f, -0.5f),
                new Vector3(-0.5f, 0.5f, 0.5f),
                new Vector3(0.5f, 0.5f, 0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, 0.5f),
                new Vector3(0.5f, -0.5f, 0.5f) };
            cubeTetMesh.SetVertices(vertices);
            int[] indices = {
                4, 1, 2, 7,
                4, 5, 1, 7,
                4, 1, 0, 2,
                1, 7, 3, 2,
                6, 4, 2, 7 };
            cubeTetMesh.SetIndices(indices);
            return cubeTetMesh;
        }

        public static Geometry GetXYPlane(int numXDivisions = 0, int numYDivisions = 0)
        {
            int numCellsX = numXDivisions + 1;
            int numCellsY = numXDivisions + 1;
            int numCells = numCellsX * numCellsY;
            int numPointsX = numXDivisions + 2;
            int numPointsY = numYDivisions + 2;
            int numPoints = numPointsX * numPointsY;
            Vector2 spacing = new Vector2(1.0f / numCellsX, 1.0f / numCellsY);

            Vector3[] vertices = new Vector3[numPoints];
            Vector2[] texCoords = new Vector2[numPoints];
            int[] indices = new int[numCells * 6];

            Geometry planeMesh = ScriptableObject.CreateInstance<Geometry>();
            planeMesh.name = "PlaneMesh";
            planeMesh.type = GeometryType.SurfaceMesh;

            int iter = 0;
            for (int j = 0; j < numPointsY; j++)
            {
                for (int i = 0; i < numPointsX; i++)
                {
                    texCoords[iter] = new Vector2(i, j) * spacing;
                    Vector2 pos = texCoords[iter] - new Vector2(0.5f, 0.5f);
                    vertices[iter++] = new Vector3(pos.x, pos.y, 0.0f);
                }
            }

            iter = 0;
            for (int j = 0; j < numCellsY; j++)
            {
                for (int i = 0; i < numCellsX; i++)
                {
                    int index1 = i * numPointsY + j;
                    int index2 = index1 + numPointsY;
                    int index3 = index1 + 1;
                    int index4 = index2 + 1;

                    if ((i % 2 == 0) ^ (j % 2 == 0))
                    {
                        indices[iter++] = index1;
                        indices[iter++] = index2;
                        indices[iter++] = index3;

                        indices[iter++] = index4;
                        indices[iter++] = index3;
                        indices[iter++] = index2;
                    }
                    else
                    {
                        indices[iter++] = index2;
                        indices[iter++] = index4;
                        indices[iter++] = index1;

                        indices[iter++] = index4;
                        indices[iter++] = index3;
                        indices[iter++] = index1;
                    }
                }
            }

            planeMesh.SetVertices(vertices);
            planeMesh.SetTexCoords(texCoords);
            planeMesh.SetIndices(indices);
            return planeMesh;
        }
    }
}