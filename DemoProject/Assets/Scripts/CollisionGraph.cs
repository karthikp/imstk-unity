﻿using System.Collections.Generic;

namespace Imstk
{
    public enum CDType
    {
        // Points to objects
        PointSetToSphere,
        PointSetToPlane,
        PointSetToCapsule,
        PointSetToSpherePicking,
        PointSetToSurfaceMesh,

        // Mesh to mesh (mesh to analytical object = mesh vertices to analytical object)
        SurfaceMeshToSurfaceMesh,
        SurfaceMeshToSurfaceMeshCCD,
        VolumeMeshToVolumeMesh,
        MeshToMeshBruteForce,

        // Analytical object to analytical object
        UnidirectionalPlaneToSphere,
        BidirectionalPlaneToSphere,
        SphereToCylinder,
        SphereToSphere
    };
    public enum CHType
    {
        None,
        Penalty,
        VirtualCoupling,
        NodalPicking,
        BoneDrilling,
        SPH,
        PBD
    };

    public class InteractionPair
    {
        public string object1Name;
        public string object2Name;
        public CDType cdType;
        public CHType chaType;
        public CHType chbType;
    }

    public class CollisionGraph
    {
        public List<InteractionPair> pairs = new List<InteractionPair>();
        public static Dictionary<string, CDType> CDTypes = new Dictionary<string, CDType>()
        {
            { "PointSet To Sphere", CDType.PointSetToSphere },
            { "PointSet To Plane", CDType.PointSetToPlane },
            { "PointSet To Capsule", CDType.PointSetToCapsule },
            { "PointSet To SpherePicking", CDType.PointSetToSpherePicking },
            { "PointSet To SurfaceMesh", CDType.PointSetToSurfaceMesh },

            { "SurfaceMesh To SurfaceMesh", CDType.SurfaceMeshToSurfaceMesh },
            { "SurfaceMesh To SurfaceMesh CD", CDType.SurfaceMeshToSurfaceMeshCCD },
            { "VolumeMesh To VolumeMesh", CDType.VolumeMeshToVolumeMesh },
            { "Mesh To Mesh", CDType.MeshToMeshBruteForce },

            { "UnidirectionalPlane To Sphere", CDType.UnidirectionalPlaneToSphere },
            { "BidirectionalPlane To Sphere", CDType.BidirectionalPlaneToSphere },
            { "Sphere To Cylinder", CDType.SphereToCylinder },
            { "Sphere To Sphere", CDType.SphereToSphere },
        };
        public static Dictionary<string, CHType> CHTypes = new Dictionary<string, CHType>()
        {
            { "None", CHType.None },
            { "Penalty", CHType.Penalty },
            { "VirtualCoupling", CHType.VirtualCoupling },
            { "NodalPicking", CHType.NodalPicking },
            { "BoneDrilling", CHType.BoneDrilling },
            { "SPH", CHType.SPH },
            { "PBD", CHType.PBD },
        };
    }
}