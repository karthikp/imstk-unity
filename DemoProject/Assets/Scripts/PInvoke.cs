﻿namespace Imstk
{
    // Pinvoke should contains all the registration functions
    // (Function calls from C++ -> C#)
    public static class PInvoke
    {
#if IMSTK_DEBUG
        public const string ImstkUnityLibName = "iMSTKUnityWrapperd";
#else
        public const string ImstkUnityLibName = "iMSTKUnityWrapper";
#endif
    }
}