﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk.Movement
{
    [AddComponentMenu("Imstk/PbdCircleMove")]
    public class PbdCircleMove : ImstkBehaviour
    {
        public GameObject objectToOrbit = null;
        private Vector3 origin = Vector3.zero;
        public double timeStep = 0.1;

        private List<Vector3> projPolarCoords = new List<Vector3>();
        private HashSet<int> fixedIndices = new HashSet<int>();

        private double t = 0.0;
        private int instanceId = -1;

        public override void ImstkStart()
        {
            // Get the intial positions of the fixed vertices
            PbdModel model = gameObject.GetComponent<PbdModel>();
            instanceId = model.GetHandle();
            origin = objectToOrbit.transform.position;

            fixedIndices = model.fixedIndices;
            foreach (int index in fixedIndices)
            {
                Vector2 polarCoord = Vector2.zero;
                Vector3 vertex = getPbdStateVertex(model.GetHandle(), index);
                Vector3 diff = vertex - origin;
                projPolarCoords.Add(new Vector3((float)Math.Atan2(diff.z, diff.x), new Vector2(diff.z, diff.x).magnitude, vertex.y));
            }

            SimulationManager.PostUpdateCallback += PostUpdate;
        }

        public void PostUpdate(EventArgs e)
        {
            // After updating, update the position of the first vertex
            int i = 0;
            foreach (int index in fixedIndices)
            {
                Vector3 projPolarCoord = projPolarCoords[i];
                Vector3 pos = new Vector3(
                     (float)Math.Sin(projPolarCoord.x - t) * projPolarCoord.y + origin.x,
                     projPolarCoord.z,
                     (float)Math.Cos(projPolarCoord.x - t) * projPolarCoord.y + origin.z);
                setPbdStateVertex(instanceId, index, pos);
                i++;
            }
            
            t += timeStep;
        }

        public override void ImstkDestroy()
        {
            SimulationManager.PostUpdateCallback -= PostUpdate;
        }

        [DllImport(PInvoke.ImstkUnityLibName)]
        private static extern void setPbdStateVertex(int objectHandle, int index, Vector3 vertex);

        [DllImport(PInvoke.ImstkUnityLibName)]
        private static extern Vector3 getPbdStateVertex(int objectHandle, int index);
    }
}