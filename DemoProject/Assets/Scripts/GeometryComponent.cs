﻿using UnityEngine;

namespace Imstk
{
    // GeometryComponent is a Unity component that seats either a Mesh (unity) or Geometry (imstk)
    public class GeometryComponent : MonoBehaviour
    {
        public void SetMesh(Geometry mesh) { this.mesh = mesh; }
        public void SetMesh(Mesh mesh) { this.mesh = mesh; }

        public Geometry GetImstkMesh() { return (Geometry)mesh; }
        public Mesh GetUnityMesh() { return (Mesh)mesh; }

        public bool IsUnityMesh { get { return mesh.GetType() == typeof(Mesh); } }
        public bool IsImstkMesh { get { return mesh.GetType() == typeof(Geometry); } }

        public bool IsVolume
        {
            get
            {
                if (mesh != null)
                    return (mesh.GetType() == typeof(Geometry) && (mesh as Geometry).IsVolume);
                else
                    return false;
            }
        }

        public Object mesh = null;
    }
}