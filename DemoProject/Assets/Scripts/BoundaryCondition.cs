﻿using UnityEngine;

namespace Imstk
{
    [RequireComponent(typeof(Transform))]
    [RequireComponent(typeof(MeshFilter))]
    [AddComponentMenu("Imstk/BoundaryCondition")]
    public class BoundaryCondition : MonoBehaviour
    {
        public GameObject bcObj = null;
    }
}