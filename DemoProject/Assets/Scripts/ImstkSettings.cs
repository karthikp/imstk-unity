﻿using UnityEditor;
using UnityEngine;

namespace Imstk
{
    public class ImstkSettings : ScriptableObject
    {
        public const string settingsPath = "Assets/Resources/ImstkSettings.asset";

        [SerializeField]
        public bool useDebug = false;

        [SerializeField]
        public bool useLogger = false;

        [SerializeField]
        public bool useOptimalNumberOfThreads = true;

        [SerializeField]
        public int numThreads = 8;

        public static ImstkSettings Instance()
        {
            var settings = AssetDatabase.LoadAssetAtPath<ImstkSettings>(settingsPath);
            // If it doesn't exist, create default
            if (settings == null)
            {
                settings = CreateInstance<ImstkSettings>();
                AssetDatabase.CreateAsset(settings, settingsPath);
            }

            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
            settings.useDebug = defines.Contains("IMSTK_DEBUG");

            AssetDatabase.SaveAssets();

            return settings;
        }
    }
}