﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    [RequireComponent(typeof(Transform))]
    [RequireComponent(typeof(MeshFilter))]
    public abstract class DynamicalModel : ImstkBehaviour
    {
        // Components
        //protected SceneObject sceneObjectComp = null;
        protected MeshFilter meshFilter = null;
        protected PhysicsGeometry physicsGeometryComp = null;
        protected CollisionGeometry collisionGeometryComp = null;
        protected Transform transformComp = null;
        public Matrix4x4 initTransform;

        protected Geometry physicsGeometry = null;
        protected int physicsGeometryHandle = -1;
        protected Geometry visualGeometry = null;
        protected int visualGeometryHandle = -1;
        protected Geometry collisionGeometry = null;
        protected int collisionGeometryHandle = -1;

        public int GetHandle() { return gameObject.GetInstanceID(); }


        public override void ImstkAwake()
        {
            // Get dependencies
            //sceneObjectComp = gameObject.GetComponentOrCreate<SceneObject>();
            meshFilter = gameObject.GetComponentFatal<MeshFilter>();
            physicsGeometryComp = gameObject.GetComponentOrCreate<PhysicsGeometry>();
            collisionGeometryComp = gameObject.GetComponentOrCreate<CollisionGeometry>();
            transformComp = gameObject.GetComponentFatal<Transform>();

            // Make sure mesh filter is read/writable
            meshFilter.mesh.MarkDynamic();
            if (!meshFilter.mesh.isReadable)
            {
                Debug.LogWarning(gameObject.name + "'s MeshFilter Mesh must be readable (check the meshes import settings)");
                return;
            }

            // Initialize the object
            if (!genDynamicObjectWithHandle(GetHandle()))
                Debug.Log("Failed to create dynamic object, handle already exists");

            InitObject();
            InitGeometry();
            ProcessBoundaryConditions(gameObject.GetComponents<BoundaryCondition>());
            Configure();

            // Reset the transform (so transform isn't applied twice)
            // Unfortunately this means any component that needs the transform will get bad values
            // Instead initTransform is provided here as the transform will be reset
            initTransform = transformComp.localToWorldMatrix;
            transformComp.localPosition = Vector3.zero;
            transformComp.localScale = Vector3.one;
            transformComp.localRotation = Quaternion.identity;
        }

        protected abstract void InitObject();

        protected abstract void Configure();

        /// <summary>
        /// Each subclassed model may *apply* boundary conditions differently
        /// </summary>
        /// <param name="conditions">All the conditions to be processed</param>
        protected virtual void ProcessBoundaryConditions(BoundaryCondition[] conditions) { }

        protected void InitGeometry()
        {
            // Setup the visual geometry
            {
                visualGeometry = Geometry.MeshToGeometry(meshFilter.mesh, transform.localToWorldMatrix);
                // Create geometry obj in c api
                visualGeometryHandle = Geometry.ExportGeometry(visualGeometry);
                setDynamicObjectVisualGeometry(GetHandle(), visualGeometryHandle);
            }

            // Setup the collision geometry
            {
                // If user wants collision = visual
                if (collisionGeometryComp.usage == CollisionGeometryUsage.USE_VISUAL_GEOMETRY)
                {
                    collisionGeometry = visualGeometry;
                    setDynamicObjectCollisionGeometry(GetHandle(), visualGeometryHandle);
                    collisionGeometryHandle = visualGeometryHandle;
                }
                // If user wants collision = physics
                else if (collisionGeometryComp.usage == CollisionGeometryUsage.USE_PHYSICS_GEOMETRY)
                    Debug.LogError("Collision geometry cannot = physics geometry yet");
                // If use own geometry
                else
                {
                    // Make a transformed copy
                    collisionGeometry = ScriptableObject.CreateInstance<Geometry>();
                    if (collisionGeometryComp.IsImstkMesh)
                    {
                        (collisionGeometryComp.mesh as Geometry).CopyTo(collisionGeometry);
                        collisionGeometry.Transform(transformComp.localToWorldMatrix);
                    }
                    else
                        collisionGeometry = Geometry.MeshToGeometry(collisionGeometryComp.mesh as Mesh, transformComp.localToWorldMatrix);

                    // Create geometry in the c api
                    collisionGeometryHandle = Geometry.ExportGeometry(collisionGeometry);
                    setDynamicObjectCollisionGeometry(GetHandle(), collisionGeometryHandle);
                }
            }

            // Setup the physics geometry
            {
                // If user wants physics = visual
                if (physicsGeometryComp.usage == PhysicsGeometryUsage.USE_VISUAL_GEOMETRY)
                {
                    physicsGeometry = visualGeometry;
                    setDynamicObjectPhysicsGeometry(GetHandle(), visualGeometryHandle);
                    physicsGeometryHandle = visualGeometryHandle;
                }
                // If use own geometry
                else
                {
                    // Make a transformed copy
                    physicsGeometry = ScriptableObject.CreateInstance<Geometry>();
                    if (physicsGeometryComp.IsImstkMesh)
                    {
                        (physicsGeometryComp.mesh as Geometry).CopyTo(physicsGeometry);
                        physicsGeometry.Transform(transformComp.localToWorldMatrix);
                    }
                    else
                        physicsGeometry = Geometry.MeshToGeometry(physicsGeometryComp.mesh as Mesh, transformComp.localToWorldMatrix);

                    // Create geometry in the c api
                    physicsGeometryHandle = Geometry.ExportGeometry(physicsGeometry);
                    setDynamicObjectPhysicsGeometry(GetHandle(), physicsGeometryHandle);
                }
            }

            // Map the physics to collision geometry
            if (collisionGeometryComp.usage == CollisionGeometryUsage.USE_OWN_GEOMETRY)
                addDynamicObjectPhysicsToCollidingMap(GetHandle());

            // Map the physics to visual geometry
            if (physicsGeometryComp.usage == PhysicsGeometryUsage.USE_OWN_GEOMETRY)
                addDynamicObjectPhysicsToVisualMap(GetHandle());
        }


        public void Update()
        {
            // We don't import the entire geometry (we don't need to copy the whole thing)
            // just the vertices. In later implementations we'll import/export and ideally couple data arrays
            visualGeometry.PinVertices();
            updateDynamicObjectVisualGeometry(GetHandle(), visualGeometry.gcVertices.AddrOfPinnedObject());
            visualGeometry.FreeVertices();

            // Copy geometry to GPU
            meshFilter.mesh.vertices = visualGeometry.vertices;
            meshFilter.mesh.RecalculateNormals();
            //meshFilter.mesh.RecalculateBounds();
        }


        public override void ImstkDestroy()
        {
            // Delete non duplicate handles
            HashSet<int> geomHandles = new HashSet<int>();
            geomHandles.Add(visualGeometryHandle);
            geomHandles.Add(physicsGeometryHandle);
            geomHandles.Add(collisionGeometryHandle);
            foreach (int val in geomHandles)
            {
                if (val != -1)
                    Geometry.deleteGeometry(val);
            }

            deleteDynamicObject(GetHandle());
        }


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern bool genDynamicObjectWithHandle(int objectHandle);


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void addDynamicObjectCollidingToVisualMap(int objectHandle);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void addDynamicObjectPhysicsToCollidingMap(int objectHandle);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void addDynamicObjectPhysicsToVisualMap(int objectHandle);


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void setDynamicObjectPhysicsGeometry(int objectHandle, int geometryHandle);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void setDynamicObjectVisualGeometry(int objectHandle, int geometryHandle);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void setDynamicObjectCollisionGeometry(int objectHandle, int geometryHandle);


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void updateDynamicObjectVisualGeometry(int objectHandle, IntPtr vertexBufferPtr);


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void deleteDynamicObject(int objectHandle);
    }
}