﻿//using Imstk;
//using System.Runtime.InteropServices;
//using UnityEngine;
//using UnityEditor;

//namespace ImstkEditor
//{
//    public class GeometricUtilityMenu : Editor
//    {
//        // Extend this to instead present the user with a list of options of all
//        // the components on the gameobject (untiy can't do modal dialog boxes though)
//        public static Geometry GetChosenGeometry(
//            MeshFilter visualGeometry,
//            PhysicsGeometry physicsGeometry,
//            CollisionGeometry collisionGeometry)
//        {
//            // Figure out what geoemtry to process
//            if (visualGeometry != null)
//            {
//                Geometry results = CreateInstance<Geometry>();
//                results.FromUnityMesh(visualGeometry.sharedMesh);
//                return results;
//            }
//            else if (physicsGeometry != null)
//            {
//                if (physicsGeometry.usage == PhysicsGeometryUsage.USE_OWN_GEOMETRY)
//                {
//                    if (physicsGeometry.IsImstkMesh)
//                        return physicsGeometry.GetImstkMesh();
//                    else
//                    {
//                        Geometry results = CreateInstance<Geometry>();
//                        results.FromUnityMesh(physicsGeometry.GetUnityMesh());
//                        return results;
//                    }
//                }
//            }
//            else if (collisionGeometry != null)
//            {
//                if (collisionGeometry.usage == CollisionGeometryUsage.USE_OWN_GEOMETRY)
//                {
//                    if (collisionGeometry.IsImstkMesh)
//                        return collisionGeometry.GetImstkMesh();
//                    else
//                    {
//                        Geometry results = CreateInstance<Geometry>();
//                        results.FromUnityMesh(collisionGeometry.GetUnityMesh());
//                        return results;
//                    }
//                }
//            }
//            return null;
//        }

//        // Strips the cells from a volume
//        [MenuItem("Imstk/VolumeToPoints")]
//        public static void VolumeToPoints()
//        {

//        }

//        [MenuItem("Imstk/LaplacianSmooth")]
//        public static void LaplacianSmooth()
//        {
//            GameObject obj = Selection.activeGameObject;
//            MeshFilter filter = obj.GetComponent<MeshFilter>();
//            PhysicsGeometry physicsGeometry = obj.GetComponent<PhysicsGeometry>();
//            CollisionGeometry collisionGeometry = obj.GetComponent<CollisionGeometry>();

//            Geometry chosenGeometry = GetChosenGeometry(filter, physicsGeometry, collisionGeometry);
//            chosenGeometry.Pin();
//            LaplacianSmooth(chosenGeometry.GetPrimGeometry());
//            chosenGeometry.Free();
//        }

//        [DllImport(PInvoke.ImstkUnityLibName)]
//        private static extern void LaplacianSmooth(PrimitiveGeometry geom);
//    }
//}