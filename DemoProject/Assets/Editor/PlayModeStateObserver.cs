﻿using Imstk;
using UnityEditor;

namespace ImstkEditor
{
    /// <summary>
    /// This class observes the play mode state to properly pause and step
    /// Imstk in the Unity editor
    /// </summary>
    [InitializeOnLoadAttribute]
    class PlayModeStateObserver
    {
        static PlayModeStateObserver()
        {
            //EditorApplication.playModeStateChanged += playModeStateChanged;
            EditorApplication.pauseStateChanged += pauseStateChanged;
        }

        private static void pauseStateChanged(PauseState state)
        {
            if (SimulationManager.simManager != null)
            {
                ModuleStatus status = SimulationManager.getSimManagerStatus();
                if (state == PauseState.Paused)
                {
                    // Simulations will get paused before quitting because of Unity
                    if (status == ModuleStatus.running)
                        SimulationManager.pauseSimManager();
                }
                else if (state == PauseState.Unpaused)
                {
                    if (status == ModuleStatus.paused)
                        SimulationManager.resumeSimManager();
                }
            }
        }

        //private static void playModeStateChanged(PlayModeStateChange state) { }
    }
}