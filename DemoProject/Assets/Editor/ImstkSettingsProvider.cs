﻿using Imstk;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    class ImstkSettingsProvider : SettingsProvider
    {
        private ImstkSettings settings;

        public ImstkSettingsProvider(string path, SettingsScope scope = SettingsScope.User) : base(path, scope) { }

        public static bool IsSettingsAvailable() { return File.Exists(ImstkSettings.settingsPath); }

        // Read existing settings or create settings if they don't exist when user clicks them
        public override void OnActivate(string searchContext, VisualElement rootElement) { settings = ImstkSettings.Instance(); }

        public override void OnGUI(string searchContext)
        {
            EditorGUI.BeginChangeCheck();
            bool useDebug = EditorGUILayout.Toggle("Use Debug", settings.useDebug);
            bool useLogger = EditorGUILayout.Toggle("Use Logger", settings.useLogger);
            bool useOptimalNumberOfThreads = EditorGUILayout.Toggle("Use Optimal # Threads", settings.useOptimalNumberOfThreads);
            int numThreads = settings.numThreads;
            if (!useOptimalNumberOfThreads)
                numThreads = EditorGUILayout.IntField("# Threads", numThreads);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(settings, "Change of Settings");
                settings.useDebug = useDebug;
                settings.useLogger = useLogger;
                settings.useOptimalNumberOfThreads = useOptimalNumberOfThreads;
                settings.numThreads = numThreads;

                string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
                defines = defines.Replace("IMSTK_RELEASE;", "");
                defines = defines.Replace("IMSTK_RELEASE", "");
                defines = defines.Replace("IMSTK_DEBUG;", "");
                defines = defines.Replace("IMSTK_DEBUG", "");
                if (defines.Length != 0 && defines[defines.Length - 1] != ';')
                    defines += ';';
                if (useDebug)
                    defines += "IMSTK_DEBUG;";
                else
                    defines += "IMSTK_RELEASE;";
                PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defines);

                AssetDatabase.SaveAssets();
            }
        }

        // Register the SettingsProvider
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            var provider = new ImstkSettingsProvider("Project/Imstk Settings", SettingsScope.Project);
            provider.keywords = new HashSet<string>(new string[] { "Debug", "Logger", "Threads", "Imstk" });
            return provider;
        }
    }
}