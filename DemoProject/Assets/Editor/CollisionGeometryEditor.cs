﻿using Imstk;
using UnityEngine;
using UnityEditor;

namespace ImstkEditor
{
    [CustomEditor(typeof(CollisionGeometry))]
    class CollisionGeometryEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            CollisionGeometry collisionGeometry = target as CollisionGeometry;
            EditorGUI.BeginChangeCheck();

            CollisionGeometryUsage usage = (CollisionGeometryUsage)EditorGUILayout.EnumPopup("Usage", collisionGeometry.usage);
            Object mesh = collisionGeometry.mesh;
            if (usage == CollisionGeometryUsage.USE_OWN_GEOMETRY)
                mesh = EditorGUILayout.ObjectField("Mesh", collisionGeometry.mesh, typeof(Object), true);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(collisionGeometry, "Change of CollisionGeometry");
                collisionGeometry.usage = usage;

                if (mesh != null)
                {
                    if (mesh.GetType() == typeof(Geometry))
                        collisionGeometry.SetMesh(mesh as Geometry);
                    else if (mesh.GetType() == typeof(Mesh))
                        collisionGeometry.SetMesh(mesh as Mesh);
                    else
                        Debug.LogWarning("Object must be a Unity Mesh or Imstk Geometry");
                }
            }
        }
    }
}