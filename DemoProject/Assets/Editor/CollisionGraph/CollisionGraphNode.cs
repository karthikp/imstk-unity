﻿using Imstk;
using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace ImstkEditor
{
    // Abstract class for nodes in the collision graph
    // All nodes in the graph must have serializable NodeData
    public abstract class CollisionGraphNode : Node
    {
        public NodeData data = null;
        public List<Port> ports = new List<Port>();

        protected void SetData(NodeType type, string title)
        {
            data.guid = Guid.NewGuid().ToString();
            this.title = data.title = title;
            data.type = type;
            data.position = new Vector2(GetPosition().x, GetPosition().y);
        }

        public override Port InstantiatePort(Orientation orientation, Direction direction, Port.Capacity capacity, Type type)
        {
            Port port = base.InstantiatePort(orientation, direction, capacity, type);
            ports.Add(port);
            return port;
        }

        public virtual NodeData GetData() { return data; }
    }
}