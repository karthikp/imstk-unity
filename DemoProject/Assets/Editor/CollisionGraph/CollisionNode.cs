﻿using Imstk;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    class CollisionNode : CollisionGraphNode
    {
        PopupField<string> chaField = null;
        PopupField<string> chbField = null;

        public CollisionNode(string title, CDType cdType, CHType chaType, CHType chbType)
        {
            data = new CollisionNodeData();
            CollisionNodeData collisionData = data as CollisionNodeData;
            SetData(NodeType.Collision, title);
            collisionData.cdType = cdType;
            collisionData.chaType = chaType;
            collisionData.chbType = chbType;
            AddCHFields();
        }
        public CollisionNode(CollisionNodeData data)
        {
            title = data.title;
            this.data = data;
            AddCHFields();
        }

        protected void AddCHFields()
        {
            CollisionNodeData collisionData = data as CollisionNodeData;
            List<string> options = new List<string>();
            foreach (KeyValuePair<string, CHType> entry in CollisionGraph.CHTypes)
            {
                options.Add(entry.Key);
            }
            chaField = new PopupField<string>(options, (int)collisionData.chaType);
            chaField.RegisterCallback((ChangeEvent<string> e) =>
            {
                (data as CollisionNodeData).chaType = CollisionGraph.CHTypes[chaField.value];
            });
            Add(chaField);
            chbField = new PopupField<string>(options, (int)collisionData.chbType);
            chbField.RegisterCallback((ChangeEvent<string> e) =>
            {
                (data as CollisionNodeData).chbType = CollisionGraph.CHTypes[chbField.value];
            });
            Add(chbField);
        }

        public override NodeData GetData()
        {
            CollisionNodeData collisionData = data as CollisionNodeData;
            collisionData.chaType = GetCHAType();
            collisionData.chbType = GetCHBType();
            return collisionData;
        }

        public CHType GetCHAType() { return CollisionGraph.CHTypes[chaField.text]; }
        public CHType GetCHBType() { return CollisionGraph.CHTypes[chbField.text]; }
    }
}