﻿using Imstk;
using UnityEditor;

namespace ImstkEditor
{
    [CustomEditor(typeof(FemModel))]
    public class FemModelEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            // Check the config file toggle for change
            FemModel modelScript = target as FemModel;
            EditorGUI.BeginChangeCheck();

            bool useConfigFile = EditorGUILayout.Toggle("Use Config File", modelScript.useConfigFile);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(modelScript, "Change of Use Config");
                modelScript.useConfigFile = useConfigFile;
            }

            if (modelScript.useConfigFile)
                updateConfigFile();
            else
                updateParameters();
        }

        public void updateConfigFile()
        {
            FemModel modelScript = target as FemModel;
            EditorGUI.BeginChangeCheck();

            string configFilePath = modelScript.configFilePath;
            if (UnityEngine.GUILayout.Button("Config File"))
                configFilePath = EditorUtility.OpenFilePanel("Config File", modelScript.configFilePath, "config");
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(modelScript, "Change of Config");
                modelScript.configFilePath = configFilePath;
            }
            EditorGUILayout.LabelField(modelScript.configFilePath);
        }

        public void updateParameters()
        {
            FemModel modelScript = target as FemModel;
            EditorGUI.BeginChangeCheck();

            FEMMethodType femMethodType = (FEMMethodType)EditorGUILayout.EnumPopup("FEM Method Type", modelScript.femMethodType);
            HyperElasticMaterialType materialType = (HyperElasticMaterialType)EditorGUILayout.EnumPopup("Material Type", modelScript.materialType);
            double dampingMassCoefficient = EditorGUILayout.DoubleField("Damping Mass Coefficient", modelScript.dampingMassCoefficient);
            double dampingStiffnessCoefficient = EditorGUILayout.DoubleField("Damping Stiffness Coefficient", modelScript.dampingStiffnessCoefficient);
            double dampingLaplacianCoefficient = EditorGUILayout.DoubleField("Damping Laplacian Coefficient", modelScript.dampingLaplacianCoefficient);
            double deformationCompliance = EditorGUILayout.DoubleField("Deformation Compliance", modelScript.deformationCompliance);
            double gravityAccel = EditorGUILayout.DoubleField("Gravity Accel", modelScript.gravityAccel);
            double compressionResistance = EditorGUILayout.DoubleField("Compression Resistance", modelScript.compressionResistance);
            double inversionThreshold = EditorGUILayout.DoubleField("Inversion Threshold", modelScript.inversionThreshold);
            bool useRealtime = EditorGUILayout.Toggle("Use Realtime", modelScript.useRealtime);
            double timeStep = modelScript.dt;
            if (!useRealtime)
                timeStep = EditorGUILayout.DoubleField("Timestep", modelScript.dt);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(modelScript, "Change of Parameters");
                modelScript.femMethodType = femMethodType;
                modelScript.materialType = materialType;
                modelScript.dampingMassCoefficient = dampingMassCoefficient;
                modelScript.dampingStiffnessCoefficient = dampingStiffnessCoefficient;
                modelScript.dampingLaplacianCoefficient = dampingLaplacianCoefficient;
                modelScript.deformationCompliance = deformationCompliance;
                modelScript.gravityAccel = gravityAccel;
                modelScript.compressionResistance = compressionResistance;
                modelScript.inversionThreshold = inversionThreshold;
                modelScript.useRealtime = useRealtime;
                modelScript.dt = timeStep;
            }
        }
    }
}