#pragma once
#include "Geometry.h"
#include "PInvoke.h"

struct RigidParams
{
    double test;
};

extern "C"
{
    ///
    /// \brief Sets the parameters of the specified RigidObject's model
    ///
    IMSTK_EXPORT void configureRigidObject(const int objectHandle, const RigidParams params);

    ///
    /// \brief Initializes a RigidObject with a FemModel.
    ///
    IMSTK_EXPORT void initRigidObject(const int objectHandle);
}