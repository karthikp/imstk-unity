#pragma once
#include <imstkInternalForceModel.h>

#include "PInvoke.h"

struct FemParams
{
    imstk::FEMMethodType femMethodType;
    imstk::HyperElasticMaterialType hyperelasticMaterialType;
    double dampingMassCoefficient;
    double dampingStiffnessCoefficient;
    double dampingLaplacianCoefficient;
    double deformationCompliance;
    double gravityAccel;
    double compressionResistance;
    double inversionThreshold;
    const char* configFilePath;
    double dt;
    int* fixedIndices;
    int fixedIndexCount;
};

extern "C"
{
    ///
    /// \brief Sets the parameters of the specified FemObject's model
    ///
    IMSTK_EXPORT void configureFemObject(const int objectHandle, const FemParams params);

    ///
    /// \brief Initializes a FemObject with a FemModel.
    ///
    IMSTK_EXPORT void initFemObject(const int objectHandle);
}