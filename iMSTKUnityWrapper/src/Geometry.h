#pragma once
// msvc16 2019 internal compiler error fix
#define _ENABLE_EXTENDED_ALIGNED_STORAGE
#include <memory>
#include <unordered_map>

#include "PInvoke.h"

namespace imstk
{
class Geometry;
} // namespace imstk

// An iterative key is used
extern int geometryIter;
extern std::unordered_map<int, std::shared_ptr<imstk::Geometry>> geometries;

///
/// \struct Specifies geometry type
///
enum GeometryType
{
    Plane,
    Sphere,
    Cylinder,
    Cube,
    Capsule,
    PointSet,
    SurfaceMesh,
    TetrahedralMesh,
    HexahedralMesh,
    LineMesh,
    Decal,
    DecalPool,
    RenderParticles,
    ImageData
};

///
/// \struct Specifies primitive geometry
///
struct GeometryInfo
{
    int vertexCount = 0;
    int cellCount = 0;
    GeometryType type = GeometryType::PointSet;
};

///
/// \struct Container for geometry, very primitive for now
///
struct PrimitiveGeometry
{
    GeometryInfo info;
    float* vertexBufferPtr = nullptr;
    int* indexBufferPtr = nullptr;
};

extern "C"
{
    ///
    /// \brief Creates a new geometry and returns the handle
    ///
    IMSTK_EXPORT int genGeometry(const GeometryType type);

    ///
    /// \brief Creates a new geometry with the given handle, returns if suceeded or not,
    /// fails if there already exists another geometry with that handle.
    ///
    IMSTK_EXPORT bool genGeometryWithHandle(const GeometryType type, const int objHandle);

    ///
    /// \brief Creates a new geometry from the given primitive geometry
    ///
    IMSTK_EXPORT int genGeometryFromPrimitiveGeometry(PrimitiveGeometry primGeometry);

    ///
    /// \brief Gets a struct with various geometry info
    ///
    IMSTK_EXPORT GeometryInfo getGeometryInfo(const int objHandle);

    ///
    /// \brief Fills the provided primGeometry buffers with the geometry from the geometry object
    /// at the handle
    ///
    IMSTK_EXPORT void exportGeometry(const int objHandle, PrimitiveGeometry primGeometry);

    ///
    /// \brief Fills a mask telling whether points are inside/outside of a surface
    ///
    IMSTK_EXPORT void testEnclosedPoints(const int surfHandle,
                                         const int pointSetHandle,
                                         bool* isInside);

    ///
    /// \brief Extracts surface mesh from volume
    ///
    IMSTK_EXPORT int extractSurfaceMesh(const int volHandle, const bool flipNormals);

    ///
    /// \brief Reverses the winding of a surface mesh
    ///
    IMSTK_EXPORT void reverseSurfaceMesh(const int surfHandle);

    ///
    /// \brief Deletes the geometry at the handle
    ///
    IMSTK_EXPORT void deleteGeometry(const int objHandle);
}

///
/// \brief Copies PrimitiveGeometry to Imstk::Geometry
///
std::shared_ptr<imstk::Geometry> primitiveToImstkGeometry(PrimitiveGeometry primGeometry);

///
/// \brief Copies Imstk::Geometry to preallocated PrimitiveGeometry, note: use imstkGeometryInfo
/// to preallocate
///
void imstkToPrimitiveGeometry(std::shared_ptr<imstk::Geometry> imstkGeometry,
                              PrimitiveGeometry& primGeometry);

///
/// \brief Returns the info of an Imstk::Geometry
///
GeometryInfo imstkGeometryInfo(std::shared_ptr<imstk::Geometry> geometry);