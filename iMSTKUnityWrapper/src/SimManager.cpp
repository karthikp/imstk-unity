#include "SimManager.h"

#include <imstkCamera.h>
#include <imstkCollisionDetection.h>
#include <imstkCollisionGraph.h>
#include <imstkDynamicObject.h>
#include <imstkScene.h>
#include <imstkSceneManager.h>
#include <imstkSimulationManager.h>

#include "DynamicObject.h"

namespace simManager
{
std::shared_ptr<imstk::SimulationManager> sdk = nullptr;
std::function<void()> PostUpdate = nullptr;

void genSimManager(const int numThreads)
{
    // Create SDK
    std::shared_ptr<imstk::SimManagerConfig> simConfig =
        std::make_shared<imstk::SimManagerConfig>();
    simConfig->simulationMode = imstk::SimulationMode::RunInBackgroundAsync;
    simConfig->threadPoolSize = numThreads;
    simConfig->enableStdoutLogging = false;
    simConfig->enableFileLogging = false;
    sdk = std::make_shared<imstk::SimulationManager>(simConfig);
    // Create the default scene
    std::shared_ptr<imstk::Scene> scene = sdk->createNewScene("Scene");
    sdk->setActiveScene(scene);
    sdk->getSceneManager(scene)->setPostUpdateCallback([&](imstk::Module* module) {
        if (PostUpdate != nullptr)
            PostUpdate();
    });
}

void initSimManager()
{
    if (sdk != nullptr)
        sdk->initialize();
    else
        LOG(WARNING) << "Cannot initialize SimulationManager, does not exist";
}

void setSimManagerPostUpdateCallback(void (*func)())
{
    PostUpdate = func;
}

void addInteractionPair(int objAInstanceId,
                        int objBInstanceId,
                        int cdType,
                        int chAType,
                        int chBType)
{
    if (!dynamicObjects.count(objAInstanceId))
    {
        LOG(INFO) << "Could not find object of instance id: " << objAInstanceId
                  << " for collision pair";
        return;
    }
    std::shared_ptr<imstk::CollidingObject> objA =
        std::dynamic_pointer_cast<imstk::CollidingObject>(dynamicObjects[objAInstanceId]);
    if (!dynamicObjects.count(objBInstanceId))
    {
        LOG(INFO) << "Could not find object of instance id: " << objBInstanceId
                  << " for collision pair";
        return;
    }
    std::shared_ptr<imstk::CollidingObject> objB =
        std::dynamic_pointer_cast<imstk::CollidingObject>(dynamicObjects[objBInstanceId]);

    /* LOG(INFO) << "Collision pair added to scene: (\n\t"
         << "CD Type: " << cdType << "\n\t"
         << "CHA Type: " << chAType << "\n\t"
         << "CHB Type: " << chBType << "\n\t"
         << "ObjA ID: " << objAInstanceId << "\n\t"
         << "ObjB ID: " << objBInstanceId;*/

    sdk->getActiveScene()->getCollisionGraph()->addInteractionPair(
        objA,
        objB,
        static_cast<imstk::CollisionDetection::Type>(cdType),
        static_cast<imstk::CollisionHandling::Type>(chAType),
        static_cast<imstk::CollisionHandling::Type>(chBType));
}

void startSimManager()
{
    if (sdk != nullptr)
        sdk->start(imstk::SimulationStatus::Running, imstk::Renderer::Mode::Empty);
    else
        LOG(FATAL) << "Cannot start SimulationManager, does not exist";
}

void advanceFrame(const double dt)
{
    sdk->getActiveScene()->advance(dt);
}

double getActiveSceneDt()
{
    return sdk->getActiveScene()->getElapsedTime();
}

void pauseSimManager()
{
    sdk->pause();
}

void resumeSimManager()
{
    sdk->run();
}

imstk::ModuleStatus getSimManagerStatus()
{
    return sdk->getStatus();
}

void stopSimManager()
{
    if (sdk != nullptr)
        sdk->end();
    else
        LOG(FATAL) << "Cannot stop SimulationManager, does not exist";
}

void deleteSimManager()
{
    sdk = nullptr;
}
} // namespace simManager