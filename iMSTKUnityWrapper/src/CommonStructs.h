#pragma once

struct Vec3f
{
    float x;
    float y;
    float z;
};

struct Vec4f
{
    float x;
    float y;
    float z;
    float w;
};

struct Mat4x4f
{
    float m[16];
};