## About

#### Overview
[iMSTK](https://www.imstk.org/) is a C++ based free & open-source toolkit that aids rapid prototyping of real-time multi-modal surgical simulation scenarios. [Unity](https://unity.com/) is a multi-platform game engine designed to create 2D and 3D games.

This repository contains imstk's Unity plugin, C# scripts, demo scenarios and resources needed to build imstk Unity package. 

#### License
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

## Resources

#### Documentation

User documentation: [https://imstk-unity.readthedocs.io/en/latest/](https://imstk-unity.readthedocs.io/en/latest/)

API documentation: https://imstk-unity.gitlab.io/documentation/

#### Issue-tracker
https://gitlab.kitware.com/iMSTK/imstk-unity/issues


## Building iMSTK-Unity

#### Prerequisites
* Git
* CMake 3.15.x
* Unity 2019.3.2f1
* Visual Studio 2019 (Not tested with older versions)
* iMSTK (Tag: [imstk-unity](https://gitlab.kitware.com/iMSTK/iMSTK/-/tags/imstk-unity))


#### Build steps

**1. Clone the source**

```git clone https://gitlab.kitware.com/iMSTK/imstk-unity.git```

**2. CMake build configuration**

```
Run cmake with:

Compiler: VS 2019 x64
source directory = <your cloned repo>/iMSTKUnityWrapper
build directory = <somewhere outside of this repo>
iMSTK_DIR = <path to your iMSTK build directory>/install/lib/cmake/iMSTK-2.0
```

**Note:** Make sure to build the version of imstk code with tag: imstk-unity as specified above

Optionally one may change the CMAKE_INSTALL_PREFIX to install to a different Unity project. It defaults to the adjacent demo project.


**3. Build and install**

Open the VS 2019 project file and build the INSTALL target in release (or debug, if you want to debug).

This will copy your built library as well as the dependencies to the install directory.